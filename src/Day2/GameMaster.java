package Day2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class GameMaster {

    private int maxRed   = 12;
    private int maxGreen = 13;
    private int maxBlue  = 14;

    public GameMaster() {
    }

    public int calcSumOfPossibleGames(String inputFilePath) throws IOException {

        int sumOfPossibleGames = 0;

        BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
        Scanner scanner1;
        String lineOfGames;
        String lineOfValues;

        while ((lineOfGames = reader.readLine()) != null){
            scanner1 = new Scanner(lineOfGames);
            scanner1.useDelimiter(":");

            lineOfValues = scanner1.next();

            Scanner scanner2 = new Scanner(lineOfValues);
            scanner2.useDelimiter(" ");
            scanner2.next();
            int gameNumber = Integer.parseInt(scanner2.next());
            scanner2.close();

            lineOfValues = scanner1.next();

            if (isGamePossible(lineOfValues)){
                sumOfPossibleGames += gameNumber;
            }

            scanner1.close();

        }


        reader.close();

        return sumOfPossibleGames;
    }

    public boolean isGamePossible(String lineOfValues) {

        Scanner scanner2 = new Scanner(lineOfValues);
        scanner2.useDelimiter(";");
        String games;

        while (scanner2.hasNext()){

            games = scanner2.next();

            Scanner scanner3 = new Scanner(games);
            scanner3.useDelimiter(",");
            String gameValue;

            while (scanner3.hasNext()){

                gameValue = scanner3.next();
                Scanner scanner4 = new Scanner(gameValue);
                scanner4.useDelimiter(" ");

                int checkValue = Integer.parseInt(scanner4.next());

                scanner4.close();


                if (gameValue.contains("red")){
                    if (checkValue > this.maxRed){
                        return false;
                    }
                } else {
                    if (gameValue.contains("green")) {
                        if (checkValue > this.maxGreen) {
                            return false;
                        }
                    } else {
                        if (gameValue.contains("blue")) {
                            if (checkValue > this.maxBlue) {
                                return false;
                            }
                        }
                    }
                }

            }

            scanner3.close();

        }

        scanner2.close();

        return true;
    }

    public int calcSumPowerOfGames(String inputFilePath) throws IOException {

        int sumPowerOfGames = 0;

        BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
        Scanner scanner1;
        String lineOfGames;
        String lineOfValues;

        while ((lineOfGames = reader.readLine()) != null){
            scanner1 = new Scanner(lineOfGames);
            scanner1.useDelimiter(":");

            lineOfValues = scanner1.next();

            Scanner scanner2 = new Scanner(lineOfValues);
            scanner2.useDelimiter(" ");
            scanner2.next();
            int gameNumber = Integer.parseInt(scanner2.next());
            scanner2.close();

            lineOfValues = scanner1.next();

            sumPowerOfGames += getPowerOfGame(lineOfValues);

            scanner1.close();

        }


        reader.close();

        return sumPowerOfGames;
    }

    public int getPowerOfGame(String lineOfValues) {

        int numberOfRed   = 0;
        int numberOfGreen = 0;
        int numberOfBlue  = 0;

        Scanner scanner2 = new Scanner(lineOfValues);
        scanner2.useDelimiter(";");
        String games;

        while (scanner2.hasNext()){

            games = scanner2.next();

            Scanner scanner3 = new Scanner(games);
            scanner3.useDelimiter(",");
            String gameValue;

            while (scanner3.hasNext()){

                gameValue = scanner3.next();
                Scanner scanner4 = new Scanner(gameValue);
                scanner4.useDelimiter(" ");

                int checkValue = Integer.parseInt(scanner4.next());

                scanner4.close();


                if (gameValue.contains("red")){
                    if (checkValue > numberOfRed){
                        numberOfRed = checkValue;
                    }
                } else {
                    if (gameValue.contains("green")) {
                        if (checkValue > numberOfGreen) {
                            numberOfGreen = checkValue;
                        }
                    } else {
                        if (gameValue.contains("blue")) {
                            if (checkValue > numberOfBlue) {
                                numberOfBlue = checkValue;
                            }
                        }
                    }
                }

            }

            scanner3.close();

        }

        scanner2.close();

        //System.out.println(numberOfRed * numberOfGreen * numberOfBlue);

        return numberOfRed * numberOfGreen * numberOfBlue;
    }
}
