package Day3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SchematicReader {

    private char[][] matrix;
    private char[][] cloneMatrix;
    private String regexNum = "\\d+";

    public SchematicReader(String inputFilePath) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(inputFilePath));
        int iterator = 0;
        String line;



        while ((line = reader.readLine()) != null){
            if (this.matrix == null){
                matrix = new char[line.length()][line.length()];
            }

            this.matrix[iterator] = line.toCharArray();

            iterator++;

        }

        reader.close();
    }

    public void printMatrix(){

        for (char[] chars : this.matrix) {

            String rowNumber = "";
            int firstDigitJ = -1;
            int firstDigitI = -1;
            int lastDigitJ = -1;
            int lastDigitI = -1;

            for (char aChar : chars) {

                System.out.print(aChar);

            }

            System.out.println();
        }
    }

    public int calcSumEngineNumber(){

        int sumEngineNumber = 0;

        for (int j = 0; j < this.matrix.length; j++){

            String rowNumber = "";
            int digitJ      = -1;
            int firstDigitI = -1;
            int lastDigitI  = -1;

            for (int i = 0; i < this.matrix[j].length; i++){

                // is a number is found, extend the rowNumber
                if(Character.isDigit(this.matrix[j][i])){

                    rowNumber += this.matrix[j][i];

                    if (firstDigitI < 0){
                        firstDigitI = i;
                        digitJ = j;
                    }

                    lastDigitI = i;

                    // we are at the end of the line, we also check
                    if (i == 139){

                        if (isSerialNumber(digitJ, firstDigitI, lastDigitI)){

                            sumEngineNumber += Integer.parseInt(rowNumber);

                            //System.out.print(rowNumber + " ");
                        }
                    }

                } else {

                    // validation of the current number, if there is a number to check
                    if (!rowNumber.equals("")){

                        if (isSerialNumber(digitJ, firstDigitI, lastDigitI)){

                            sumEngineNumber += Integer.parseInt(rowNumber);

                            //System.out.print(rowNumber + " ");

                        }
                    }

                    digitJ = firstDigitI = lastDigitI  = -1;
                    rowNumber = "";
                }
            }

            //System.out.println();
        }

        return sumEngineNumber;
    }

    private boolean isSerialNumber(int digitJ, int firstDigitI, int lastDigitI){

        // check left side, if left I is in range
        if (firstDigitI - 1 >= 0){

            // upper left, if in range
            if (digitJ - 1 >= 0){

                if (!(this.matrix[digitJ - 1][firstDigitI - 1] == '.' || Character.isDigit(this.matrix[digitJ - 1][firstDigitI - 1]))){
                    return true;
                }
            }

            // left value
            if (!(this.matrix[digitJ][firstDigitI - 1] == '.' || Character.isDigit(this.matrix[digitJ][firstDigitI - 1]))){
                return true;
            }

            // lower left, if in range
            if (digitJ + 1 < this.matrix.length){

                if (!(this.matrix[digitJ + 1][firstDigitI - 1] == '.' || Character.isDigit(this.matrix[digitJ + 1][firstDigitI - 1]))){
                    return true;
                }
            }
        }

        // check above, if in range
        if (digitJ - 1 >= 0){
            for (int a = firstDigitI; a <= lastDigitI; a++){

                if (!(this.matrix[digitJ - 1][a] == '.' || Character.isDigit(this.matrix[digitJ - 1][a]))){
                    return true;
                }
            }
        }

        // check below, if in range
        if (digitJ + 1 < this.matrix.length){
            for (int a = firstDigitI; a <= lastDigitI; a++){

                if (!(this.matrix[digitJ + 1][a] == '.' || Character.isDigit(this.matrix[digitJ + 1][a]))){
                    return true;
                }
            }
        }

        // check right side, if right I is in range
        if (lastDigitI + 1 < this.matrix.length){

            // upper right, if in range
            if (digitJ - 1 >= 0){

                if (!(this.matrix[digitJ - 1][lastDigitI + 1] == '.' || Character.isDigit(this.matrix[digitJ - 1][lastDigitI + 1]))){
                    return true;
                }
            }

            // left value
            if (!(this.matrix[digitJ][lastDigitI + 1] == '.' || Character.isDigit(this.matrix[digitJ][lastDigitI + 1]))){
                return true;
            }

            // lower left, if in range
            if (digitJ + 1 < this.matrix.length){

                return !(this.matrix[digitJ + 1][lastDigitI + 1] == '.' || Character.isDigit(this.matrix[digitJ + 1][lastDigitI + 1]));
            }
        }

        return false;
    }

    public int calcSumGearRatios(){

        int sumGearRatios = 0;

       this.cloneMatrix = this.matrix;

        //find gears (*) in cloned matrix
        for (int j = 0; j < this.cloneMatrix.length; j++){

            for (int i = 0; i < this.cloneMatrix[j].length; i++){

                if (this.cloneMatrix[j][i] == '*'){

                    sumGearRatios += getGearRatio(j, i);

                }
            }
        }

        return sumGearRatios;
    }

    private int getGearRatio(int gearJ, int gearI){

        int numOfGearNumbers = 0;
        int gearRatio = 1;

        // check values surrounding the gear, are there any numbers?

        // check left
        if (gearI - 1 >= 0){

            if (gearJ - 1 >= 0){
                if (Character.isDigit(this.cloneMatrix[gearJ - 1][gearI - 1])){
                    gearRatio *= maskGearNumber(gearJ - 1, gearI - 1);
                    numOfGearNumbers++;
                }
            }

            if (Character.isDigit(this.cloneMatrix[gearJ][gearI - 1])){
                gearRatio *= maskGearNumber(gearJ, gearI - 1);
                numOfGearNumbers++;
            }

            if (gearJ + 1 < this.cloneMatrix.length){
                if (Character.isDigit(this.cloneMatrix[gearJ + 1][gearI - 1])){
                    gearRatio *= maskGearNumber(gearJ + 1, gearI - 1);
                    numOfGearNumbers++;
                }
            }
        }

        // check below
        if (gearJ + 1 < this.cloneMatrix.length){
            if (Character.isDigit(this.cloneMatrix[gearJ + 1][gearI])){
                gearRatio *= maskGearNumber(gearJ + 1, gearI);
                numOfGearNumbers++;
            }
        }

        // check above
        if (gearJ - 1 >= 0){
            if (Character.isDigit(this.cloneMatrix[gearJ - 1][gearI])){
                gearRatio *= maskGearNumber(gearJ - 1, gearI);
                numOfGearNumbers++;
            }
        }

        // check right
        if (gearI + 1 < this.cloneMatrix.length){

            if (gearJ - 1 >= 0){
                if (Character.isDigit(this.cloneMatrix[gearJ - 1][gearI + 1])){
                    gearRatio *= maskGearNumber(gearJ - 1, gearI + 1);
                    numOfGearNumbers++;
                }
            }

            if (Character.isDigit(this.cloneMatrix[gearJ][gearI + 1])){
                gearRatio *= maskGearNumber(gearJ, gearI + 1);
                numOfGearNumbers++;
            }

            if (gearJ + 1 < this.cloneMatrix.length){
                if (Character.isDigit(this.cloneMatrix[gearJ + 1][gearI + 1])){
                    gearRatio *= maskGearNumber(gearJ + 1, gearI + 1);
                    numOfGearNumbers++;
                }
            }
        }

        // refresh cloneMatrix
        this.cloneMatrix = this.matrix;

        // only if exactly two gear numbers were found, the gear is considered valid
        if (numOfGearNumbers == 2){
            return gearRatio;
        } else {
            return 0;
        }
    }

    private int maskGearNumber(int gearJ, int gearI){

        String gearNumber = "";
        int i = gearI;

        // first we search for the start of the number
        while (i >= 0 && Character.isDigit(this.cloneMatrix[gearJ][i])){


            i--;

        }

        // one step back, and we have the first number
        i++;

        // find the number and mask the field in the clone matrix along the way, this is done to prevent later check of the same gear
        // from finding the same number again
        while (i < this.cloneMatrix.length && Character.isDigit(this.cloneMatrix[gearJ][i])){

            gearNumber += this.cloneMatrix[gearJ][i];
            this.cloneMatrix[gearJ][i] = '?';

            i++;

        }

        return Integer.parseInt(gearNumber);
    }
}
