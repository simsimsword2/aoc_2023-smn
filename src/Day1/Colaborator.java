package Day1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Colaborator {

    public int calcTotalCalibrationValue(String filePath)
            throws IOException {

        int totalCalibrationValue = 0;

        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;

        while ((line = reader.readLine()) != null){

            int firstInt  = -1;
            int latestInt = -1;

            for (int i = 0; i < line.length(); i ++){

                if (Character.isDigit(line.charAt(i))){

                    latestInt = Character.getNumericValue(line.charAt(i));

                    if (firstInt < 0){
                        firstInt = latestInt;
                    }
                }
            }

            //System.out.println(firstInt + " " + latestInt);

            totalCalibrationValue += firstInt*10 + latestInt;
        }


        reader.close();

        return totalCalibrationValue;
    }

    public int calcTotalCalibrationValue2(String filePath)
            throws IOException {

        int totalCalibrationValue = 0;
        int iterator = 0;

        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line, testString = "";

        while ((line = reader.readLine()) != null){

            iterator ++;

            int firstInt  = -1;
            int latestInt = -1;

            for (int i = 0; i < line.length(); i ++){

                if (Character.isDigit(line.charAt(i))){

                    testString = "";
                    latestInt = Character.getNumericValue(line.charAt(i));

                    if (firstInt < 0){
                        firstInt = latestInt;
                    }

                } else {

                    testString += line.charAt(i);

                    if (testString.contains("one")){
                        latestInt = 1;
                        testString = "";
                    } else {
                        if (testString.contains("two")){
                            latestInt = 2;
                            testString = "";
                        } else {
                            if (testString.contains("three")){
                                latestInt = 3;
                                testString = "";
                            } else {
                                if (testString.contains("four")) {
                                    latestInt = 4;
                                    testString = "";
                                } else {
                                    if (testString.contains("five")) {
                                        latestInt = 5;
                                        testString = "";
                                    } else {
                                        if (testString.contains("six")) {
                                            latestInt = 6;
                                            testString = "";
                                        } else {
                                            if (testString.contains("seven")) {
                                                latestInt = 7;
                                                testString = "";
                                            } else {
                                                if (testString.contains("eight")){
                                                    latestInt = 8;
                                                    testString = "";
                                                } else {
                                                    if (testString.contains("nine")){
                                                        latestInt = 9;
                                                        testString = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (testString.equals("")){
                        if (firstInt < 0){
                            firstInt = latestInt;
                        }
                    }
                }
            }

            System.out.println(iterator + ": " + firstInt + "" + latestInt);
            //System.out.println(firstInt + "" + latestInt);

            totalCalibrationValue += firstInt*10 + latestInt;
        }


        reader.close();

        return totalCalibrationValue;
    }

    public static void test(){
        long sum=0;
        try{
            BufferedReader br=new BufferedReader(new FileReader("src/Day1/input"));
            sum=br.lines().map(s->s.replaceAll("one","o1ne").replaceAll("two","t2wo")
                    .replaceAll("three","t3hree").replaceAll("four","f4our").replaceAll("five","f5ive")
                    .replaceAll("six","s6ix").replaceAll("seven","s7even").replaceAll("eight","e8ight")
                    .replaceAll("nine","n9ine").replaceAll("[a-z]",""))
                    .mapToInt(s->(s.charAt(0)-'0')*10+s.charAt(s.length()-1)-'0').sum();
            br.close();
        }catch(Exception e){System.out.println(e.toString());}
        System.out.println(sum);
    }

    public static void test2() throws IOException {

        BufferedReader br=new BufferedReader(new FileReader("src/Day1/input"));
        String line, testString = "";
        String testLine = "";
        int iterator = 0;

        while ((line = br.readLine()) != null){

            iterator ++;

            String value = line.replaceAll("one","o1ne").replaceAll("two","t2wo")
                    .replaceAll("three","t3hree").replaceAll("four","f4our").replaceAll("five","f5ive")
                    .replaceAll("six","s6ix").replaceAll("seven","s7even").replaceAll("eight","e8ight")
                    .replaceAll("nine","n9ine").replaceAll("[a-z]","");
            //System.out.println(iterator + ": " + value);
            //System.out.println(value.charAt(0) + "" + value.charAt(value.length()-1));
        }

    }


}
