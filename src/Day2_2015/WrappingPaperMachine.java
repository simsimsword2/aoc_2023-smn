package Day2_2015;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class WrappingPaperMachine {

    public WrappingPaperMachine() {

    }

    public int calcTotalWrappingPaperFromOrderFile(String filePath)
            throws IOException {

        int totalWrappingPaperAmount = 0;

        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        Scanner scanner;
        String line;
        int length = 0, width = 0, height = 0;

        while ((line = reader.readLine()) != null){
            scanner = new Scanner(line);
            scanner.useDelimiter("x");

            while (scanner.hasNext()){
                length = scanner.nextInt();
                width  = scanner.nextInt();
                height = scanner.nextInt();
            }

            scanner.close();

            //System.out.println(length + " " + width + " " + height + "\n"); //just for debugging

            // l * w
            int value1 = length * width;

            // l * h
            int value2 = length * height;

            // w * h
            int value3 = width * height;

            totalWrappingPaperAmount += 2*value1 + 2*value2 + 2*value3 + Math.min(value1,Math.min(value2, value3));

        }


        reader.close();

        return totalWrappingPaperAmount;
    }

    public int calcTotalRibbonFromOrderFile(String filePath)
            throws IOException {

        int totalRibbonAmount = 0;

        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        Scanner scanner;
        String line;
        int length = 0, width = 0, height = 0;

        while ((line = reader.readLine()) != null){
            scanner = new Scanner(line);
            scanner.useDelimiter("x");

            while (scanner.hasNext()){
                length = scanner.nextInt();
                width  = scanner.nextInt();
                height = scanner.nextInt();
            }

            scanner.close();

            //System.out.println(length + " " + width + " " + height + "\n"); //just for debugging

            // find and store the two smallest values
            int small1 = Math.min(length, width);
            int larger1 = Math.max(length, width);
            int small2 = Math.min(larger1, height);

            totalRibbonAmount += 2*small1 + 2*small2 + length * width * height;

        }


        reader.close();

        return totalRibbonAmount;
    }
}
