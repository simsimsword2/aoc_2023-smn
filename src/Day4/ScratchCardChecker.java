package Day4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ScratchCardChecker {

    private String filePath;
    private int length;

    public ScratchCardChecker(String filePath) throws IOException {
        this.filePath = filePath;

        int i = 0;
        BufferedReader reader = new BufferedReader(new FileReader(this.filePath));

        while (reader.readLine() != null){
            i++;
        }

        reader.close();
        this.length = i;

    }

    public int calcSumCardPoints() throws IOException {

        int sumCardPoints = 0;
        ArrayList<String> wList;
        ArrayList<String> yList;

        BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
        String game;

        while ((game = reader.readLine()) != null){

            Scanner scanner1 = new Scanner(game);
            scanner1.useDelimiter(":");
            scanner1.next();
            String numbers = scanner1.next();
            scanner1.close();

            Scanner scanner2 = new Scanner(numbers);
            scanner2.useDelimiter("\\|");
            String wNumStr = scanner2.next();
            String yNumStr = scanner2.next();
            scanner2.close();

            wList = strToArray(wNumStr);
            yList = strToArray(yNumStr);
            int winningCounter = -1;

            for (String number : yList){

                if (wList.contains(number)){
                    winningCounter++;
                }
            }

            if (winningCounter > -1){
                sumCardPoints += Math.pow(2,winningCounter);
            }
        }
        reader.close();

        return sumCardPoints;
    }

    private ArrayList<String> strToArray(String inputStr){

        ArrayList<String> result = new ArrayList<>();

        Scanner scan = new Scanner(inputStr);
        scan.useDelimiter("\\s+");

        while (scan.hasNext()){

            result.add(scan.next());
        }

        scan.close();

        return result;
    }

    public int calcSumCardStacker() throws IOException {

        int sumCardStack = 0;
        ArrayList<String> wList;
        ArrayList<String> yList;
        int[] countList = new int[this.length];
        Arrays.fill(countList, 0);

        BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
        String game;
        int gameCounter = 0;

        while ((game = reader.readLine()) != null){

            countList[gameCounter]++;

            Scanner scanner1 = new Scanner(game);
            scanner1.useDelimiter(":");
            scanner1.next();
            String numbers = scanner1.next();
            scanner1.close();

            Scanner scanner2 = new Scanner(numbers);
            scanner2.useDelimiter("\\|");
            String wNumStr = scanner2.next();
            String yNumStr = scanner2.next();
            scanner2.close();

            wList = strToArray(wNumStr);
            yList = strToArray(yNumStr);
            int winningCounter = 0;

            for (String number : yList){

                if (wList.contains(number)){

                    winningCounter++;
                    countList[gameCounter + winningCounter] += countList[gameCounter];
                }
            }

            gameCounter++;
        }

        reader.close();

        for (int i = 0; i < this.length; i++){
            sumCardStack += countList[i];
        }

        return sumCardStack;
    }
}
